import {Topic} from "./lib/Queue/Message/types";
import {queueService} from "./lib/Queue/queue";
import {tryPayloadFromString} from "./lib/Queue/Message/PlaybackChanged";
import StorageFactory from "./services/StorageFactory";
import config from "./config";
import * as path from "path";

const storage = new StorageFactory().getStorage(config.storageProvider);
queueService().then(async (queue) => {
    await queue.connect();

    const playbackTopic = Topic.playback;
    await queue.subscribe(playbackTopic);
    await queue.listen((topic, message) => {
        if (topic === playbackTopic) {
            if (tryPayloadFromString(message)) {
                storage.saveFile(path.join(
                    __dirname,
                    config.queue.storageFilename), message, '')
            }
        }
    });
});
