import * as fs from 'fs';
import * as path from 'path';
import config from '../config'

const axios = require('axios');
class LocalFileStorage implements IStorage {
  private imgSubdir = config.env === 'development' ? '../public/images' : 'public/images';
  private imgDirPath = path.resolve(__dirname, this.imgSubdir);

  constructor() {
    if (!fs.existsSync(this.imgDirPath)) {
      fs.mkdirSync(this.imgDirPath, {recursive: true});
    }
  }

  public async getRandomFile() {
    const publicPath = `${config.protocol}${config.host}:${config.serverPort}`
    const imgUrlSlug = 'images';
    const files = await fs.promises.readdir(this.imgDirPath);
    const index = Math.floor(Math.random() * files.length)
    return `${publicPath}/${imgUrlSlug}/${files[index]}`;
  }

  public async getFile(filename:string) {
    const publicPath = `${config.protocol}${config.host}:${config.serverPort}`
    const imgUrlSlug = 'images';
    return `${publicPath}/${imgUrlSlug}/${filename}`;
  }
  /**
   *
   * @param filepath
   * @param contents
   * @param extension 'including prepended .'
   */
  async saveImage(filepath: string, contents: any, extension: string|undefined) {
    const filename = path.basename(filepath) + extension;
    const files = await fs.promises.readdir(this.imgDirPath);

    if (files.find(element => element === filename)) {
      return;
    }

    if (files.length >= config.maxFilesToStore) {
      this.deleteRandomFile(files);
    }

    await fs.readFile(filename, (err, file) => {
      fs.writeFile(`${this.imgDirPath}/${filename}`, contents, 'base64', (err) => {
        if (err) console.error(err);
      });
    });

    return;
  }

  async saveFile(filepath: string, contents: any, extension: string|undefined) {
    const file = `${filepath}${extension ? extension : ''}`;
    await fs.promises.writeFile(file, contents, {flag: 'w+'});
  }

  private async deleteRandomFile(files: Array<string>) {
    const index = Math.floor(Math.random() * 10 / files.length);
    fs.unlink(path.resolve(`${this.imgDirPath}/${files[index]}`), (err) => {
      if (err) console.error(err);
    });
  }
}
export default LocalFileStorage;
