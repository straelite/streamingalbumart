import axios from "axios"

export const readFile = async(remoteFileName :string): Promise<string> => {
    const fileContents = await axios.get(remoteFileName, { responseType: 'arraybuffer' });
    let buffer = Buffer.from(fileContents.data, 'binary').toString('base64');
    buffer = buffer.replace(/^data:image\/\w+;base64,/, '');
    return buffer
}
