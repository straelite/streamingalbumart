interface IStorage {
    saveImage(filename :string, contents: any, extension: string|undefined): Promise<void>
    saveFile(filename :string, contents: any, extension: string|undefined): Promise<void>
    getRandomFile(): Promise<string>
    getFile(filename: string): Promise<string>;
}
