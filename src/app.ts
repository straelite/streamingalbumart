import config from './config'
const express = require('express');
const os = require('os');
const fs = require('fs');
const qs = require('querystring')
const axios = require('axios');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

const scheme = config.protocol;
const host = config.host
const port = config.serverPort;
const redirectUri = `${scheme}${host}:${port}/token-verified`;

app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.resolve(__dirname, 'public')));
app.set('view engine', 'ejs');

app.get('/', async (req, res) => {
    if (config.env === 'development') {
        res.render(path.resolve(__dirname, 'resources', 'index'))
    } else {
        res.sendFile(path.resolve(__dirname, './index.html'))
    }
})

app.get('/network-info', (req, res) => {
    const networkInterfaces = os.networkInterfaces();
    res.json(networkInterfaces);
});

app.get('/spotify-token', (req, res) => {
    const scopes = 'user-read-currently-playing';
    res.redirect('https://accounts.spotify.com/authorize' +
        '?response_type=code' +
        '&client_id=' + config.clientId +
        (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
        '&redirect_uri=' + encodeURIComponent(redirectUri));
});

app.get('/token-verified', (req, res) => {
    if (!req.query.code || !req.query.code.length) {
        res.send('Spotify did not return code')
        return;
    }

    const buffer = Buffer.from(`${config.clientId}:${config.clientSecret}`);
    axios({
        url: "https://accounts.spotify.com/api/token",
        method: "post",
        data: qs.stringify({
            grant_type: "authorization_code",
            code: req.query.code,
            redirect_uri: redirectUri
        }),
        headers: {
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            'Authorization': 'Basic ' + buffer.toString('base64')
        }
    }).then((response) => {
        if (response.data.access_token) {
            fs.writeFileSync(path.resolve(__dirname, './tokens.json'), JSON.stringify({
                'accessToken': response.data.access_token,
                'refreshToken': response.data.refresh_token
            }));
            res.send('ok!')

        }
    }).catch((err) => {
        console.log(err);
        res.send('error. check logs');
    });
});

app.listen(port);
