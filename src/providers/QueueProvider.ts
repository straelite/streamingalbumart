import IProvider from "./IProvider";
import StorageFactory from "../services/StorageFactory";
import config from "../config";
import * as fsp from 'fs/promises'
import * as path from "path";
import FallbackProvider from "./FallbackProvider";
import {MessagePlaybackPayload} from "../lib/Queue/Message/PlaybackChanged";
import {readFile} from "../services/ExternalFileHandler";

export class QueueProvider implements IProvider {

    private stored = config.env === 'development' ? path.join(__dirname, '..', config.queue.storageFilename) : path.join(__dirname, config.queue.storageFilename)
    async getImage(): Promise<string> {
        try {
            const contents = await fsp.readFile(this.stored, 'utf-8')
            const mostRecent = JSON.parse(contents) as MessagePlaybackPayload;
            if (!mostRecent.play || !mostRecent.trk?.img.lrg) {
                return new FallbackProvider().getImage();
            }
            const storage = new StorageFactory().getStorage(config.storageProvider);
            const fileContents = await readFile(mostRecent.trk?.img.lrg);

            storage.saveImage(mostRecent.trk?.img.lrg, fileContents, '.jpeg');
            return mostRecent.trk?.img.lrg
        } catch (e) {
            console.error(`Failed to load image from queue. Falling back. Got err ${e?.message}`)
            return new FallbackProvider().getImage();
        }
    }

}
