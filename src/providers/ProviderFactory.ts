import IProvider from "./IProvider";
import SpotifyProvider from "./SpotifyProvider";
import FallbackProvider from "./FallbackProvider"
import {QueueProvider} from "./QueueProvider";

class ProviderFactory {
    public getProvider(providerName: string): IProvider {
        switch (providerName) {
            case "queue":
                return new QueueProvider
            case "spotify":
                return new SpotifyProvider

            default:
                return new FallbackProvider
        }
    }
}

export default ProviderFactory;
