import config from "../config";
import IProvider from './IProvider'
import ISpotifyToken from '../models/ISpotifyToken';
import FallbackProvider from "./FallbackProvider";
import StorageFactory from "../services/StorageFactory";
import {readFile} from "../services/ExternalFileHandler";

const axios = require('axios');
const fs = require('fs');
const qs = require('querystring');
const path = require('path');
const tokenPath = config.env === 'development' ? path.resolve(__dirname, '../tokens.json') : path.resolve(__dirname, './tokens.json');

class SpotifyProvider implements IProvider {
    private accessToken: string;
    private activeImage: string;

    constructor() {
        this.accessToken = this.readTokenFile().accessToken
    }

    async getImage() {
        if (!this.accessToken.length) {
            console.log("no access token. Clear interval");
            throw Error("no access token");
        }
        try {
            const response = await axios({
                url: 'https://api.spotify.com/v1/me/player/currently-playing',
                method: 'get',
                headers: {
                    'Authorization': 'Bearer ' + this.accessToken
                }
            })

            if (!response.data || !response.data.item || !response.data.item.album.images) {
                return await new FallbackProvider().getImage();
            }

            this.activeImage = response.data.item.album.images[0].url;
            const factory = new StorageFactory();
            const fileHandler = factory.getStorage(config.storageProvider);
            const fileContents = await readFile(this.activeImage);

            fileHandler.saveImage(this.activeImage, fileContents,'.jpeg');
            return this.activeImage;

        } catch (err) {
            if (err.response && err.response.status && err.response.status === 401) {
                this.refreshAccessToken();
            }
            console.error(err)
        }
    }

    private refreshAccessToken() {
        const tokenObj = this.readTokenFile();
        if (!tokenObj.refreshToken || !tokenObj.refreshToken.length) {
            console.error('No refresh token');
        }

        const buffer = Buffer.from(`${config.clientId}:${config.clientSecret}`);

        axios({
            method: 'post',
            url: 'https://accounts.spotify.com/api/token',
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': 'Basic ' + buffer.toString('base64')
            },
            data: qs.stringify({
                grant_type: 'refresh_token',
                refresh_token: tokenObj.refreshToken
            }),
            json: true
        }).then((response) => {
            const refreshToken = response.data.refresh_token && response.data.refresh_token.length ? response.data.refresh_token : tokenObj.refreshToken
            fs.writeFileSync(tokenPath, JSON.stringify({
                'accessToken': response.data.access_token,
                'refreshToken': refreshToken
            }), 'utf8');
        }).catch((err) => {
            console.error(err.message);
        });
    }

    private readTokenFile(): ISpotifyToken {
        try {
            const fileContents = fs.readFileSync(tokenPath, 'utf8');
            const tokenObj = JSON.parse(fileContents);
            return tokenObj;
        } catch {
            console.error('unable to read access code');
        }
    };
}

export default SpotifyProvider;
