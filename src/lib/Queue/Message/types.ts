enum Topic {
    playback = "spotify_playback",
    heartbeat = "heartbeat",
    connected = "connected",
    envStatus = "env_status"
}

interface IMessage {
    body: string,
    name: string,
    topic: Topic
}

export {
    IMessage,
    Topic
}
