import {IMessage, Topic} from "./types";
import config from "../../../config"
const Heartbeat = (data: unknown = null): IMessage => {
    const msg = {
        process: config.processName,
    }

    return {
        topic: Topic.heartbeat,
        body: JSON.stringify(msg),
        name: 'Heartbeat'
    }
}

export {
    Heartbeat
}
