import {IMessage, Topic} from "./types";
import config from "../../../config"
const Connected = (data: unknown = null):IMessage => {
    return  {
        topic: Topic.connected,
        body:  JSON.stringify({process: config.processName}),
        name: Topic.connected.toString()
    }
}

export {
    Connected
}
