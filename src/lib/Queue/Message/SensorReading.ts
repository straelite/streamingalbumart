import { IMessage, Topic } from "./types";

type sensorReadingPayload = {
  room: string;
  temp: {
    v: number;
    u: string;
  };
  humid: {
    v: number;
    u: string;
  };
  light: {
    v: number;
    u: string;
  };
};

const tryPayloadFromString = (
  serialized: string,
): sensorReadingPayload | null => {
  try {
    const parsed = JSON.parse(serialized);

    const { room, light, humid, temp } = parsed;

    // A little longform, but keeps TS quiet
    return { room, light, humid, temp };
  } catch (e) {
    console.error(
      `[${__filename}] Could not convert string to payload object. Got: ${serialized}`,
    );
    return null;
  }
};

const SensorReading = (data: sensorReadingPayload): IMessage => {
  return {
    body: JSON.stringify(data),
    topic: Topic.envStatus,
    name: "sensor_reading",
  };
};

export { SensorReading, tryPayloadFromString, sensorReadingPayload };
