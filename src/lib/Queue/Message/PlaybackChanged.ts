import {IMessage, Topic} from "./types";
//Payload size is important on the consuming devices hence simple structure and abbrev property names
type trackInformation = {
    artist: string,
    name: string,
    album: string
    img: {
        lrg: string,
        med: string
    }

}
type MessagePlaybackPayload = {
    play: boolean,
    trk: null|trackInformation
}
const playbackChanged = (data: MessagePlaybackPayload):IMessage => {
    return {
        topic: Topic.playback,
        body: JSON.stringify(data),
        name: 'playback_changed'
    }
}

const tryPayloadFromString = (serialized: string): MessagePlaybackPayload|null => {
    try {
        const parsed = JSON.parse(serialized);

        const { play, trk } = parsed;

        // A little longform, but keeps TS quiet
        return { play, trk };
    } catch (e) {
        console.error(
            `[${__filename}] Could not convert string to payload object. Got: ${serialized}`,
        );
        return null;
    }
}

export {
    tryPayloadFromString,
    MessagePlaybackPayload,
    playbackChanged
}
