import {IMessage} from "../../Message/types";
import {Connected} from "../../Message/Connected";
import config from '../../../../config';
import {IQueueService} from "../../types";

const mqtt = require("mqtt");

const {host, user, password} = config.queue.mqtt.broker
const mqttService = async (): Promise<IQueueService> => {
    const isConnected = false;
    let client;


    const connect = async (): Promise<void> => {
        if (isConnected) {
            console.log(['MQTT Service: connect() called when connection already exists. Bailing'])
            return;
        }
        client = await mqtt.connect(host, {
            auth: `${user}:${password}`
        });

        client.on("connect", () => {
            client.subscribe("presence", (err) => {
                if (!err) {
                    publish(Connected())
                }
            });
        });

        client.on('close', function () {
            console.log('Disconnected')
        })

        client.on('disconnect', function (packet) {
            console.log(`Disconnected by ${packet})`);
        })
    }

    const publish = async (message: IMessage): Promise<void> => {
        if (config.logLevel === 'verbose') {
            console.log('publishing', message.body)
        }
        await client.publish(message.topic, message.body)
    }

    const subscribe = async (topic: string, callback: any | undefined): Promise<void> => {
        client.subscribe(topic, (err) => {
            if (!err) {
                console.log(`Successfully subscribed to topic: ${topic}`);
                if (callback) {
                    callback();
                }
            } else {
                console.log(`Failed to subscribe to topic: ${topic}, Got ${err}`);
            }
        });
    }

    const listen = async (callback: (topic: string, message: string) => void): Promise<void> => {
        if (!client) {
            console.error('MQTT client not connected, cannot listen');
        }

        client.on("message", (topic: string, message: string) => {
            if (callback) {
                callback(topic, message)
            }
        })
    }


    return {
        connect,
        publish,
        subscribe,
        listen
    }
}

export default mqttService
