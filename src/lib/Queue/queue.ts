import { IQueueService } from "./types";
import config from "../../config";
import mqtt from "./Driver/mqtt";
const driver = config.queue.driver
const queueService = async (): Promise<IQueueService> => {
  switch (driver) {
    default:
      return mqtt();
  }
};

export { queueService };
