import {IMessage} from "./Message/types";

type IQueueService = {
    publish(message: IMessage): Promise<void>
    connect(): Promise<void>
    subscribe(topic: string, callback?: any|undefined): Promise<void>
    listen(callback: (topic: string, message: string) => void): Promise<void>
}

export {
    IQueueService
}
