require('dotenv').config({path: __dirname + '/.env'});
export default {
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    env: process.env.ENVIRONMENT,
    timerInterval: 6000,
    maxFilesToStore: 100,
    storageProvider: 'local',
    queue: {
        driver: 'mqtt',
        storageFilename: 'queue.json',
        mqtt: {
            broker: {
                host: process.env.MQTT_BROKER_HOST || "mqtt://127.0.0.1:1883",
                user: process.env.MQTT_BROKER_USER,
                password: process.env.MQTT_BROKER_PASS,
            },
        }
    },
    logLevel: 'normal',
    //When accessing from dev machine localhost works fine. If accessing the page from a network location, the ip of the host machine is needed
    //Config works as expected for Raspberry Pi 0 network hardware. May need updating for other devices if wlan0 is not an interface
    //host: process.env.ENVIRONMENT === 'development' ? 'localhost' : os.networkInterfaces().wlan0[0].address,
    host: 'localhost',
    serverPort: 3000,
    socketPort: 3030,
    protocol: 'http://',
    processName: "StreamingAlbumArt"
};

