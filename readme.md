# Streaming Album Art Gallery

A node js app to serve album art. Target hardware is a Rasberry Pi 0. The application serves a static webpage that connects to a websocket server to recieve updates when the playing album art changes. If the currently selected provider is not playing any album art, the service will instead display locally saved art.
When using external audio providers e.g. Spotify, album art will be saved to the configured storage provider up to a configured limit. This art is what will be used for the fallback provider.

## Aims
* An exploration of Typescript particularly in node
* Extensiblity - using interfaces and factory pattern to allow new services to be added relatively easily

### Why did you bundle the Express server?
Compression, if an application is the tip of the Iceberg, node modules are the bit that sink the titanic. As this is targetting an embedded device, I wanted a smaller footprint, bundling only what's actually used.

# Hardware Assumptions

This project was written to be used with a Rasberry Pi (0 wifi), connected to an LCD screen running a frameless broser in kiosk mode. Attempts have been made to generalise, but the script was ultimately written with a specific hardware setup in mind. The `setup` folder contains a autostart file that should be placed in `~/.config/lxsession/LXDE-pi` on the host device.

# Running the project

To run this, you will need a Spotify Developer account and have created a new app in their [Developer Dashboard](https://developer.spotify.com/dashboard/login). You will need to create an app and note the Client Id and Client Secret

* Install dependencies with `npm install`
* Copy .env.example to .env and populate
* CLIENT_ID and CLIENT_SECRET are taken from your app from the Spotify Dev console

### Running in devlopment

The command `npm run dev` will bring up the web app and websockets server locally with hot reloading. For this to work correctly, `ENVIRONMENT` in `.env` must be set to 'development' This will bring up an express webserver at localhost:3000 (default) and a websocket server at localhost:3030 (default). By default index.html expects the websocket server to be at localhost:3030 and will need to be updated.

### Running in Production
For production, the bundled version of the app is designed to be run on the client device to minimise payload and this can be obtained by running the command `npm run build`. This will produce a `dist` folder. The contents of which will be referenced in the next section.  

You must authenticate the app with any account you wish to use to grab 'currently playing' data. To do this, run app.js.
You can do this with `node app.js`. If running on something other than a Raspberry Pi, you may run into issues if your app environment is something other than 'development'. See Gotchas.

app.js will run an express server hosting a web page at port 3000 and /spotify-token on the client device (e.g 192.168.69:3000/spotify-token). 
Giving the app access to your Spotify data will redirect you back to /token-verified. At this point, your tokens.json file will be created and populated.

socket.js handles running the websockets server that will push the currently playing image to the client webpage and can be run using `node socket.js`. By default, it will check if the currently configured audio provider has audio running, and if not will use the fallback provider and cycle through stored images. 

# Configuring the project
Most configuration is handled within the .env file
However, within config.ts you will want to update the below properties:
* timerInterval (integer) is the interval in ms that the script will ping the Spotify API. 
* maxFilesToStore: (integer) the max number of files to store for the fallback provider before beginning to overwrite,
* storageProvider: (string) the configured storage provider
* host: string (string) the domain to serve the app from
* serverPort: (integer) the port that the express server will host to
* socketPort: (integer) the port that the websockets server will host to
* protocol: (string) the protocol http/(s) that the express server will serve from

# Endpoints
GET `/`: Returns: html. Shows the page that connects to the websockets server to display album art
POST `/set-provider`: Accepts: json. Request body is the string identifier of the new provider id to use Returns: json. The new active provider
GET `/network-info` Returns: json. Returns information on the network devices of the current host, used for debug.
GET `/spotify-token` Returns: redirect. Redirects the user to authenticate with the configured Spotify app
GET `/token-verified` Returns: html. User is sent here after verifying with Spotify, success or failiure displayed  (needs to be extended to support multiple providers); 


# Gotchas
* If trying to run app.js on something other than a Raspberry Pi, in production mode, you will see an error relating to 
`os.networkInterfaces().wlan0[0].address` not being set. This is a default wireless network device on the RPi 0 Wifi. 
* * To address this, set ENVIRONMENT in .env to 'development' and run `node app.js`. Then visit localhost:3000/network-info. From there you will be able to find the network devices available on your current hardware. Replace `.wlan0[0]` with your available network device.
* * Change your ENVIRONMENT value back to the previous value 

# Managing the processes
An ideal way to run this script is using the PM2 node package to manage the processes. [PM2](https://pm2.keymetrics.io/)

PM2 allows you to run run and monitor both the app.js and socket.js scripts simultaneously. It is also possible to have the scripts autolaunch if the device loses power or otherwise has to restart.

See [This guide](https://futurestud.io/tutorials/pm2-restart-processes-after-system-reboot) for instructions on how to use PM2 to run the scripts.
